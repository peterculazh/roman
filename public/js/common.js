$(function () {
    $(function () {
        /*  On submit from taking value of input and check if it empty, if true show error message and stop script.
            If true make array and encode in JSON format and send to handler
            If AJAX return error we are display error message and hide text about convert if it were show before
            If AJAX return data we are checking if empty data for more additional verification of output data
            If verification return true display error message and hide answer if it were show before and stop script
            If verification return false parsing JSON into Object and output it and show text about convert
         */
        $(".roman-form-submit").click(function () {
            var input = $(".roman-form-input").val();
            var inputArray = {"text": input};
            var jsonData = JSON.stringify(inputArray);
            var inp = $(".roman-form-input").val();
            if ($.trim(inp).length == "") {
                $(".roman-error").slideDown();
                $(".roman-error-text").text("Вы ничего не ввели");
                $(".roman-answer").slideUp();
                return false;
            }
            $.ajax({
                url: "/handler.php",
                method: "POST",
                data: {
                    input: jsonData,
                },
                error: function () {
                    $(".roman-answer").slideUp();
                    $(".roman-error").slideDown();
                    $(".roman-error-text").text("Некорректно введено");
                },
                success: function (data) {
                    if (data === '') {
                        $(".roman-error").slideDown();
                        $(".roman-error-text").text("Некорректно введено");
                        return false;
                    }
                    output = JSON.parse(data);
                    $(".roman-form-output").text(output["result"]);
                    $(".roman-answer").slideDown().text(output["text"]);
                    $(".roman-error").slideUp();
                }
            });
        });
    });
});