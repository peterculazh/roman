<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Controller\ConverterController;

if ($_POST['input']) {
    $converter = new ConverterController();
    $result = $converter->reception(json_decode($_POST['input']));
}
