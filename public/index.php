<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <title>Конвертер </title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#000">

    <link rel="stylesheet" href="css/main.min.css">

</head>

<body>

<!-- Custom HTML -->

<div class="container roman-container">

    <div class="row">

        <div class="col-12 ">
            <div class="roman-legend">
                <h1 class="roman-legend-title">Конвертер римских и арабских чисел</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="roman-answer">Конвертированно из римских цифр в арабские</div>
            <div class="roman-error">
                Ошибка: <span class="roman-error-text">Вы ввели неправильно</span>
            </div>
        </div>
    </div>

    <form action="" method="post" class="roman-form row">

        <div class="roman-form-group col-sm-12 col-md-5">
            <textarea name="input" class="roman-form-textarea roman-form-input" placeholder="Введите арабское или римское число"></textarea>
        </div>

        <div class="roman-form-action col-sm-12 col-md-2">
            <button type="button" class="roman-form-submit"></button>
        </div>
        <div class="roman-form-group col-sm-12 col-md-5">
            <textarea name="output" class="roman-form-textarea roman-form-output" disabled></textarea>
        </div>

    </form>


</div>

</div>

<script src="js/scripts.min.js "></script>
<script src="js/common.js"></script>

</body>

</html>