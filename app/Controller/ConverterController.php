<?php

namespace App\Controller;

use App\Numeric;

class ConverterController
{


    /**
     * @param $object
     * Just receive input get values from JSON type and send to function who will check num system to convert and receive result
     * And echo result to AJAX
     */
    public function reception($object)
    {
        $input = array_values((array)$object);
        $result = $this->num_system($input[0]);
        echo $result;
    }

    /**
     * @param $array
     * Checking which number system where used in form by regex and call function and give to function variable that contain numbers
     * Then inithiate array in which put result and text about type of convert and encoding in JSON format and return back to browser
     */
    public function num_system($string)
    {
        $numericClass = new Numeric();
        $numerics = $numericClass->numerics;
        if (preg_match_all('/[0-9]/', $string)) {
            $numbers = (int)$string;
            $result = array();
            $result["result"] = $this->numbers2roman($numbers, $numerics);
            $result["text"] = "Конвертированно из арабских цифр в римские";
            return json_encode($result,JSON_UNESCAPED_UNICODE);
        } elseif (preg_match_all('/\b[0IVXLCDM]+\b/i', $string)) {
            $result = array();
            $result["result"] = $this->roman2numbers($string, $numerics);
            $result["text"] = "Конвертированно из римских цифр в арабские";
            if (!isset($result)) {
                return false;
            }
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        } else {
            return false;
        }
    }

    /**
     * @param $string
     * @param $numerics
     * 1. Inithiate variables @int $sum and @int $prev_number
     * 2. Set string to upper register to not get problem with comapring letters in array and recived string
     * 3. Then loop full string and comparing each letter with roman letters in array, if compare then add arab number to sum
     * 4. Then arab number saves in $prev_number to compare on next loop
     * 5. If in the next loop arab_num large than prev_number, then subtract from arab number the previous number.
     * And from sum subtract prev number and add arab number and break loop at that place and go next loop
     */
    public function roman2numbers($string, $numerics)
    {
        $sum = 0;
        $prev_number = 0;
        $string = strtoupper($string);
        for ($start = 0; $start < strlen($string); $start++) {
            foreach ($numerics as $word => $arab_num) {
                $part = substr($string, $start, 1);
                if ($part == $word) {
                    if ($prev_number != 0) {
                        if ($arab_num > $prev_number) {
                            $arab_num -= $prev_number;
                            $sum -= $prev_number;
                            $sum += $arab_num;
                            continue;
                        }
                    }
                    $sum += $arab_num;
                    $prev_number = $arab_num;
                }
            }
        }
        return $sum;
    }

    /**
     * @param $number
     * @param $numerics
     * 1. This function convert from arabic numbers into roman numbers
     * 2. For first inithiate empty @string $string outside of function and return it after all complete. But it's can be not final work.
     * 3. For second inithiate empty @string $old_part what will use for comparing old part and new part and if it's equal look 5'th point of table
     * 4. Create variable @array $only_letters which will using to set letter which need to place in string
     * 5. Inithiate @int $counter to increase one more if $old_part and $part are equal.
     */
    public function numbers2roman($number, $numerics)
    {
        $string = "";
        $old_part = "";
        $only_letters = array_keys($numerics);
        foreach ($numerics as $word => $arab_num) {
            $counter = 1;
            $remain = $number / $arab_num;
            if ($remain >= 1) {
                $string .= str_repeat($word, floor($remain));
                $number -= floor($remain) * $arab_num;
            }
            for ($start = 0; $start < strlen($string); $start++) {
                $part = substr($string, $start, 1);
                $old_part2 = substr($string, $start - 2, 1);
                if ($part == $old_part) {
                    $counter++;
                } else {
                    $old_part = $part;
                }

                if ($counter == 4) {
                    $array_pos = array_search($part, $only_letters);
                    $old_string = str_repeat($part, 4);
                    if (isset($only_letters[$array_pos - 1])) {
                        $new_string = $part . $only_letters[$array_pos - 1];
                        $string = str_replace($old_string, $new_string, $string);
                    }
                }
            }
        }
        return $string;
    }
}

require __DIR__ . '/../../vendor/autoload.php';