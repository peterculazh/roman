<?php

namespace App;

class Numeric
{
    public $numerics = array("M" => 1000, "D" => 500, "C" => 100, "L" => 50, "X" => 10, "IX" => 9, "V" => 5, "I" => 1);
}

require __DIR__ . '/../vendor/autoload.php';